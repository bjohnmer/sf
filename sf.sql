-- MySQL dump 10.13  Distrib 5.5.43, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: sf
-- ------------------------------------------------------
-- Server version	5.5.43-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `concepto_miembro`
--

DROP TABLE IF EXISTS `concepto_miembro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `concepto_miembro` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `monto` double NOT NULL,
  `descripción` text COLLATE utf8_unicode_ci NOT NULL,
  `estatus` enum('Activo','Inactivo') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Activo',
  `miembro_id` int(10) unsigned NOT NULL,
  `concepto_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `conceptos_miembros_miembro_id_foreign` (`miembro_id`),
  KEY `conceptos_miembros_concepto_id_foreign` (`concepto_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `concepto_miembro`
--

LOCK TABLES `concepto_miembro` WRITE;
/*!40000 ALTER TABLE `concepto_miembro` DISABLE KEYS */;
INSERT INTO `concepto_miembro` VALUES (1,7000,'Sueldos y salarios','Activo',1,1,'2014-07-22 19:11:39','2014-07-24 19:26:04'),(2,1000,'Pago de Alquiler','Activo',1,2,'2014-07-23 19:05:27','2014-08-18 19:09:59'),(8,2500,'Pago de escolaridad','Activo',1,2,'2014-07-24 18:38:49','2014-07-24 18:38:49'),(9,7000,'Sueldo','Activo',11,1,'2014-07-24 19:18:41','2014-07-24 19:18:41'),(10,2000,'Gastos varios','Activo',11,2,'2014-07-29 01:03:02','2014-09-17 23:14:15');
/*!40000 ALTER TABLE `concepto_miembro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conceptos`
--

DROP TABLE IF EXISTS `conceptos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conceptos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripción` text COLLATE utf8_unicode_ci NOT NULL,
  `tipo` enum('Ingreso','Egreso') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Ingreso',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conceptos`
--

LOCK TABLES `conceptos` WRITE;
/*!40000 ALTER TABLE `conceptos` DISABLE KEYS */;
INSERT INTO `conceptos` VALUES (1,'Cuentas por Cobrar','Cuentas por Cobrar','Ingreso','2014-07-17 18:56:41','2014-07-17 18:56:41'),(2,'Cuentas por Pagar','Cuentas por Pagar','Egreso','2014-07-17 18:56:54','2014-07-17 18:57:03'),(3,'Pago de Empleados','Pago de Empleados','Egreso','2014-07-24 19:22:30','2014-07-24 19:22:30');
/*!40000 ALTER TABLE `conceptos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `familias`
--

DROP TABLE IF EXISTS `familias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `familias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dirección` text COLLATE utf8_unicode_ci NOT NULL,
  `telf` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `estatus` enum('Activo','Inactivo') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Activo',
  `usuario_id` int(10) unsigned NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fam.jpg',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `familias_usuario_id_foreign` (`usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `familias`
--

LOCK TABLES `familias` WRITE;
/*!40000 ALTER TABLE `familias` DISABLE KEYS */;
INSERT INTO `familias` VALUES (1,'Garcés','La beatriz ','02712351310','Activo',1,'zExJgWmj5NToBStMNqKA.jpg','2014-07-17 18:38:10','2014-09-17 23:13:14'),(2,'','','','Activo',2,'fam.jpg','2014-10-01 20:12:04','2014-10-01 20:12:04'),(3,'','','','Activo',3,'fam.jpg','2015-07-15 21:05:24','2015-07-15 21:05:24');
/*!40000 ALTER TABLE `familias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metas`
--

DROP TABLE IF EXISTS `metas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripción` text COLLATE utf8_unicode_ci NOT NULL,
  `metabs` double(15,2) NOT NULL,
  `fecha_limite` date NOT NULL DEFAULT '2014-07-17',
  `prioridad` int(10) unsigned NOT NULL DEFAULT '0',
  `miembro_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `metas_miembro_id_foreign` (`miembro_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metas`
--

LOCK TABLES `metas` WRITE;
/*!40000 ALTER TABLE `metas` DISABLE KEYS */;
INSERT INTO `metas` VALUES (1,'Comprar Playstation 3',22000.00,'2014-08-31',4,1,'2014-07-17 19:02:10','2014-07-21 23:23:52'),(2,'jka jhdkjha kdh akhdkha ks',400.00,'2014-08-15',30,1,'2014-07-17 19:44:39','2014-09-17 21:58:05'),(3,'Comprar Nevera',70000.00,'2014-10-31',66,1,'2014-07-17 19:52:57','2014-09-17 21:58:38');
/*!40000 ALTER TABLE `metas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `miembros`
--

DROP TABLE IF EXISTS `miembros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `miembros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `telf` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `parentesco` enum('Padre','Madre','Hij@','Herman@','Ti@','Abuel@','Otro') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Padre',
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estatus` enum('Activo','Inactivo') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Activo',
  `familia_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `miembros_familia_id_foreign` (`familia_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `miembros`
--

LOCK TABLES `miembros` WRITE;
/*!40000 ALTER TABLE `miembros` DISABLE KEYS */;
INSERT INTO `miembros` VALUES (1,'José Miguel','Briceño Garcés','garces_jm@hotmail.com','324214321','Padre','awCrIKWOuTX85ov0Y7LX.jpg','Activo',1,'2014-07-17 18:55:56','2014-09-17 21:51:58'),(11,'Laura Isabel','Berríos Ramires','sicomaniat@hotmail.com','23456789','Madre','Hlzaln5i35P42O5i2nyc.JPG','Activo',1,'2014-07-23 20:04:07','2014-09-17 21:52:54'),(12,'José Miguel','gasjdhgjaskd','jahgsdghjas@jsdhgfjds.com','22343421','Hij@','V6OSIxqrUfVF5RKRQrUV.jpg','Activo',1,'2014-09-24 22:19:22','2014-09-24 22:19:22');
/*!40000 ALTER TABLE `miembros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_04_21_201157_create_users_table',1),('2014_04_21_203655_create_concept_table',1),('2014_04_21_203917_create_family',1),('2014_04_28_184518_create_members_table',1),('2014_04_28_185710_create_goals_table',1),('2014_04_28_190723_create_conceptmembers_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `login` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `clave` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `rol` enum('Administrador','Usuario') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Usuario',
  `estatus` enum('Activo','Inactivo') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Activo',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'José Miguel','garces_jm@hotmail.com','$2y$10$n29lU9OL.zyjuxIpab9SbeNlmxVrLA5rNZw.6eXIi3cVzPPY2RQ7i','garces_jm@hotmail.com','Administrador','Activo','2014-07-17 18:38:10','2014-09-17 21:40:08'),(2,'jose miguel','th3_luck_jme18@hotmail.com','$2y$10$Ofg7yTe.6ZYOl.d1ufj1Cu6u6HRtHbkzo8.zKUq9PnQzTM/ZSRKvK','th3_luck_jme18@hotmail.com','Usuario','Activo','2014-10-01 20:12:04','2014-10-01 20:12:04'),(3,'Johnmer Bencomo','bjohnmer@gmail.com','$2y$10$Blk640LyGS5he5PSbsU8yO5aLok1UPfXnL1M9W.LCn.LSr9um85DS','bjohnmer@gmail.com','Usuario','Activo','2015-07-15 21:05:24','2015-07-15 21:48:42');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'sf'
--

--
-- Dumping routines for database 'sf'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-15 22:00:30
