<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'BaseController' => $baseDir . '/app/controllers/BaseController.php',
    'Concepto' => $baseDir . '/app/models/Concepto.php',
    'ConceptoMiembro' => $baseDir . '/app/models/ConceptoMiembro.php',
    'ConceptosController' => $baseDir . '/app/controllers/ConceptosController.php',
    'ConceptosMiembrosController' => $baseDir . '/app/controllers/ConceptosMiembrosController.php',
    'CreateConceptTable' => $baseDir . '/app/database/migrations/2014_04_21_203655_create_concept_table.php',
    'CreateConceptmembersTable' => $baseDir . '/app/database/migrations/2014_04_28_190723_create_conceptmembers_table.php',
    'CreateFamily' => $baseDir . '/app/database/migrations/2014_04_21_203917_create_family.php',
    'CreateGoalsTable' => $baseDir . '/app/database/migrations/2014_04_28_185710_create_goals_table.php',
    'CreateMembersTable' => $baseDir . '/app/database/migrations/2014_04_28_184518_create_members_table.php',
    'CreateUsersTable' => $baseDir . '/app/database/migrations/2014_04_21_201157_create_users_table.php',
    'DashboardController' => $baseDir . '/app/controllers/DashboardController.php',
    'Dashboard\\Controller\\Usuario' => $baseDir . '/app/controllers/Usuario.php',
    'DatabaseSeeder' => $baseDir . '/app/database/seeds/DatabaseSeeder.php',
    'Familia' => $baseDir . '/app/models/Familia.php',
    'FamiliasController' => $baseDir . '/app/controllers/FamiliasController.php',
    'HomeController' => $baseDir . '/app/controllers/HomeController.php',
    'IlluminateQueueClosure' => $vendorDir . '/laravel/framework/src/Illuminate/Queue/IlluminateQueueClosure.php',
    'Meta' => $baseDir . '/app/models/Meta.php',
    'MetasController' => $baseDir . '/app/controllers/MetasController.php',
    'Miembro' => $baseDir . '/app/models/Miembro.php',
    'MiembrosController' => $baseDir . '/app/controllers/MiembrosController.php',
    'SessionHandlerInterface' => $vendorDir . '/symfony/http-foundation/Symfony/Component/HttpFoundation/Resources/stubs/SessionHandlerInterface.php',
    'TestCase' => $baseDir . '/app/tests/TestCase.php',
    'User' => $baseDir . '/app/models/User.php',
    'Usuario' => $baseDir . '/app/models/Usuario.php',
);
